package com.binary_studio.tree_max_depth;

import java.util.*;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department root) {

		// Check each node using Stack.
		// Use HashMap to track depth of each Department.
		// Return max from Hashmap values.

		if (root == null) {
			return 0;
		}
		Map<Department, Integer> map = new HashMap<>();
		map.put(root, 1);

		Stack<Department> stack = new Stack<>();
		stack.push(root);

		while (!stack.isEmpty()) {

			Department current = stack.pop();

			current.subDepartments.stream().filter(Objects::nonNull).forEach(department -> {
				map.put(department, map.get(current) + 1);
				stack.push(department);
			});
		}

		return Collections.max(map.values());
	}

}
