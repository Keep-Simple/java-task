package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip dockedShip;

	private final PositiveInteger capacityMax;

	private final PositiveInteger hullMax;

	private final PositiveInteger shieldMax;

	public CombatReadyShip(DockedShip dockedShip, PositiveInteger capacityMax, PositiveInteger hullMax,
			PositiveInteger shieldMax) {
		this.dockedShip = dockedShip;
		this.capacityMax = capacityMax;
		this.hullMax = hullMax;
		this.shieldMax = shieldMax;
	}

	@Override
	public void startTurn() {
		// TODO: Ваш код здесь :)

	}

	@Override
	public void endTurn() {

		// == Calculate theoretical restored capacity ==
		Integer cap = this.dockedShip.getCapacitorAmount().value() + this.dockedShip.getCapacitorRechargeRate().value();

		// == If restored is more than max - set max ==
		if (cap > this.capacityMax.value()) {
			this.dockedShip.setCapacitorAmount(this.capacityMax);
		}
		else {
			this.dockedShip.setCapacitorAmount(PositiveInteger.of(cap));
		}
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {

		// == Check if we have enough battery ==
		if (this.dockedShip.getCapacitorAmount().value() < this.dockedShip.getAttackSubsystem()
				.getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		// == Purchase attack ==
		this.dockedShip.setCapacitorAmount(PositiveInteger.of(this.dockedShip.getCapacitorAmount().value()
				- this.dockedShip.getAttackSubsystem().getCapacitorConsumption().value()));

		// == Supply AttackAction ==
		var action = new AttackAction(this.dockedShip.getAttackSubsystem().attack(target), this, target,
				this.dockedShip.getAttackSubsystem());
		return Optional.of(action);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {

		// == Passively reduce attack with DefensiveSubsystem ==
		attack = this.dockedShip.getDefenciveSubsystem().reduceDamage(attack);
		int damageReceived = attack.damage.value();

		// == Check if ship will survive the attack ==
		if (this.dockedShip.getShieldHP().value() + this.dockedShip.getHullHP().value() <= damageReceived) {
			return new AttackResult.Destroyed();
		}

		// == Firstly take hitPoints from SHIELD and Then from HULL ==
		if (this.dockedShip.getShieldHP().value() - damageReceived < 0) {

			// == If shieldHP wasn't enough to take all Damage ==
			this.dockedShip.setShieldHP(PositiveInteger.of(0));

			this.dockedShip.setHullHP(PositiveInteger
					.of(this.dockedShip.getHullHP().value() + this.dockedShip.getShieldHP().value() - damageReceived));
		}
		else {
			// == If shield was enough to take all Damage ==
			this.dockedShip.setShieldHP(PositiveInteger.of(this.dockedShip.getShieldHP().value() - damageReceived));
		}

		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damageReceived), attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		// == Check if ship has enough Capacitor ==
		if (this.dockedShip.getCapacitorAmount().value() < this.dockedShip.getDefenciveSubsystem()
				.getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		// == Purchase regeneration ==
		this.dockedShip.setCapacitorAmount(PositiveInteger.of(this.dockedShip.getCapacitorAmount().value()
				- this.dockedShip.getDefenciveSubsystem().getCapacitorConsumption().value()));

		// == Prevent over regeneration ==
		RegenerateAction regenerateAction = this.dockedShip.getDefenciveSubsystem().regenerate();
		int hullRegenerated = regenerateAction.hullHPRegenerated.value();
		int shieldRegenerated = regenerateAction.shieldHPRegenerated.value();

		// == Check hullHP ==
		Integer hull = hullRegenerated + this.dockedShip.getHullHP().value();

		// == If restored is more than max - set max ==
		if (hull > this.hullMax.value()) {
			// == Modify final hullRegen ==
			hullRegenerated = this.hullMax.value() - this.dockedShip.getHullHP().value();
			this.dockedShip.setHullHP(this.hullMax);
		}
		else {
			this.dockedShip.setHullHP(PositiveInteger.of(hull));
		}

		// == Check shieldHP ==
		Integer shield = shieldRegenerated + this.dockedShip.getShieldHP().value();

		// == If restored is more than max - set max ==
		if (shield > this.shieldMax.value()) {
			// == Modify final shieldRegen ==
			shieldRegenerated = this.shieldMax.value() - this.dockedShip.getShieldHP().value();
			this.dockedShip.setShieldHP(this.shieldMax);
		}
		else {
			this.dockedShip.setShieldHP(PositiveInteger.of(shield));
		}

		return Optional
				.of(new RegenerateAction(PositiveInteger.of(shieldRegenerated), PositiveInteger.of(hullRegenerated)));
	}

	@Override
	public String getName() {
		return this.dockedShip.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.dockedShip.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.dockedShip.getSpeed();
	}

}
