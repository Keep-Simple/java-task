package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powerGridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		DockedShip dockedShip = new DockedShip();
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		dockedShip.setName(name);
		dockedShip.setShieldHP(shieldHP);
		dockedShip.setHullHP(hullHP);
		dockedShip.setPowerGridOutput(powerGridOutput);
		dockedShip.setCapacitorAmount(capacitorAmount);
		dockedShip.setCapacitorRechargeRate(capacitorRechargeRate);
		dockedShip.setSize(size);
		dockedShip.setSpeed(speed);
		return dockedShip;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

		if (subsystem == null) {
			// == Return PG back to ship, if module was taken off ==
			if (this.attackSubsystem != null) {
				setPowerGridOutput(PositiveInteger
						.of(this.powerGridOutput.value() + this.attackSubsystem.getPowerGridConsumption().value()));
			}
			this.attackSubsystem = null;
			return;
		}

		// == Check if ship has enough PG ==
		if (subsystem.getPowerGridConsumption().value() > this.powerGridOutput.value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.powerGridOutput.value());
		}
		// == Assign subsystem ==
		this.attackSubsystem = subsystem;
		setPowerGridOutput(
				PositiveInteger.of(this.powerGridOutput.value() - subsystem.getPowerGridConsumption().value()));
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {

		if (subsystem == null) {
			// == Give PG back to ship, if module was taken off ==
			if (this.defenciveSubsystem != null) {
				setPowerGridOutput(PositiveInteger
						.of(this.powerGridOutput.value() + this.defenciveSubsystem.getPowerGridConsumption().value()));
			}
			this.defenciveSubsystem = null;
			return;
		}
		// == Check if ship has enough PG ==
		if (subsystem.getPowerGridConsumption().value() > this.powerGridOutput.value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.powerGridOutput.value());
		}
		// == Assign subsystem ==
		this.defenciveSubsystem = subsystem;
		setPowerGridOutput(
				PositiveInteger.of(this.powerGridOutput.value() - subsystem.getPowerGridConsumption().value()));
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {

		// == Check if all subsystems are fitted ==
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}

		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}

		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		// == End of check ==

		// == Return equipped ship with Modules and needed Characteristics ==
		CombatReadyShip ship = new CombatReadyShip(this, this.capacitorAmount, this.hullHP, this.shieldHP);

		return ship;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public void setShieldHP(PositiveInteger shieldHP) {
		this.shieldHP = shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public void setHullHP(PositiveInteger hullHP) {
		this.hullHP = hullHP;
	}

	public PositiveInteger getPowerGridOutput() {
		return this.powerGridOutput;
	}

	public void setPowerGridOutput(PositiveInteger powerGridOutput) {
		this.powerGridOutput = powerGridOutput;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public void setCapacitorAmount(PositiveInteger capacitorAmount) {
		this.capacitorAmount = capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public void setCapacitorRechargeRate(PositiveInteger capacitorRechargeRate) {
		this.capacitorRechargeRate = capacitorRechargeRate;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public void setAttackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public void setDefenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public void setSpeed(PositiveInteger speed) {
		this.speed = speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public void setSize(PositiveInteger size) {
		this.size = size;
	}

}
