package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powerGridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
		defenciveSubsystem.setName(name);
		defenciveSubsystem.setPowerGridConsumption(powerGridConsumption);
		defenciveSubsystem.setCapacitorConsumption(capacitorConsumption);
		defenciveSubsystem.setImpactReductionPercent(impactReductionPercent);
		defenciveSubsystem.setShieldRegeneration(shieldRegeneration);
		defenciveSubsystem.setHullRegeneration(hullRegeneration);
		return defenciveSubsystem;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {

		// == Check if Percent is valid ==
		if (this.impactReductionPercent.value() > 95) {
			setImpactReductionPercent(PositiveInteger.of(95));
		}

		// == Take Percent from incomingDamage ==
		double damage = incomingDamage.damage.value();
		damage = damage * (100 - this.impactReductionPercent.value()) / 100;

		// == Add one if needed ==
		damage = Math.ceil(damage);

		// == Construct attackAction with new damage ==
		AttackAction attackAction = new AttackAction(PositiveInteger.of((int) damage), incomingDamage.attacker,
				incomingDamage.target, incomingDamage.weapon);

		return attackAction;
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPowerGridConsumption(PositiveInteger powerGridConsumption) {
		this.powerGridConsumption = powerGridConsumption;
	}

	public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
		this.capacitorConsumption = capacitorConsumption;
	}

	public void setImpactReductionPercent(PositiveInteger impactReductionPercent) {
		this.impactReductionPercent = impactReductionPercent;
	}

	public void setShieldRegeneration(PositiveInteger shieldRegeneration) {
		this.shieldRegeneration = shieldRegeneration;
	}

	public void setHullRegeneration(PositiveInteger hullRegeneration) {
		this.hullRegeneration = hullRegeneration;
	}

	public PositiveInteger getImpactReductionPercent() {
		return this.impactReductionPercent;
	}

	public PositiveInteger getShieldRegeneration() {
		return this.shieldRegeneration;
	}

	public PositiveInteger getHullRegeneration() {
		return this.hullRegeneration;
	}

}
