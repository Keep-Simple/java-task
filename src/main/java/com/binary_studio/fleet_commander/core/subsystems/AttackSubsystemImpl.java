package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powerGridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		attackSubsystem.setName(name);
		attackSubsystem.setPowerGridConsumption(powerGridConsumption);
		attackSubsystem.setCapacitorConsumption(capacitorConsumption);
		attackSubsystem.setOptimalSpeed(optimalSpeed);
		attackSubsystem.setOptimalSize(optimalSize);
		attackSubsystem.setBaseDamage(baseDamage);
		return attackSubsystem;
	}

	@Override
	public PositiveInteger attack(Attackable target) {

		// == Count damage according to provided formula ==
		double first = target.getSize().value() >= this.optimalSize.value() ? 1
				: target.getSize().value().doubleValue() / this.optimalSize.value().doubleValue();

		double second = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: this.optimalSpeed.value().doubleValue() / (2 * target.getCurrentSpeed().value().doubleValue());

		double damage = this.baseDamage.value() * Math.min(first, second);

		// == Add one if needed ==
		damage = Math.ceil(damage);

		return PositiveInteger.of((int) damage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPowerGridConsumption(PositiveInteger powerGridConsumption) {
		this.powerGridConsumption = powerGridConsumption;
	}

	public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
		this.capacitorConsumption = capacitorConsumption;
	}

	public void setOptimalSpeed(PositiveInteger optimalSpeed) {
		this.optimalSpeed = optimalSpeed;
	}

	public void setOptimalSize(PositiveInteger optimalSize) {
		this.optimalSize = optimalSize;
	}

	public void setBaseDamage(PositiveInteger baseDamage) {
		this.baseDamage = baseDamage;
	}

}
